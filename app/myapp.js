/**
 * Created by power on 14/10/16.
 */

var express = require('express');
var mongoose = require('mongoose');
var app = express();
var Schema = mongoose.Schema;

var host_mongo =  process.env.MONGO_HOST || "localhost";
var port_mongo = process.env.MONGO_PORT || "27018";

// var mymongo = mongoose.connect("mongodb://" + host_mongo + ":"+ port_mongo + "/mymongo");
var mymongo = mongoose.connect("mongodb://mymongo/mymongo");

var catSchema = new Schema({
    name: String,
    date: {
        type: Date,
        default: Date.now
    }
});

var CatModel = mongoose.model('Cat', catSchema);


app.get('/cat/new/:name', function (req, res) {
    console.log(req.params.name);
    var myCat = new CatModel({
        name: req.params.name
    });
    myCat.save();
    res.send('Hello World!');
});

app.get('/cat/view/:name', function (req, res) {
    CatModel.find({
        name: req.params.name
    },
    function (err, cats) {
        if(err)
            console.log("error");

        res.send(cats);
    });
});

app.listen(process.env.APP_PORT || 3000, function () {
    var port = process.env.APP_PORT || 3000;
    console.log('Example app listening on port ' + port);
});